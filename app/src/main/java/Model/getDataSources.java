package Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class getDataSources {

    @Expose
    @SerializedName("StudentMarks")
    public String StudentMarks;
    @Expose
    @SerializedName("StudentName")
    public String StudentName;
    @Expose
    @SerializedName("StudentId")
    public String StudentId;

    public String getStudentMarks() {
        return StudentMarks;
    }

    public void setStudentMarks(String StudentMarks) {
        this.StudentMarks = StudentMarks;
    }

    public String getStudentName() {
        return StudentName;
    }

    public void setStudentName(String StudentName) {
        this.StudentName = StudentName;
    }

    public String getStudentId() {
        return StudentId;
    }

    public void setStudentId(String StudentId) {
        this.StudentId = StudentId;
    }

}
