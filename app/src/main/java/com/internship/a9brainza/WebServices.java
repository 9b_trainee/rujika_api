package com.internship.a9brainza;

import android.app.ProgressDialog;
import android.graphics.Movie;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;
import java.util.concurrent.TimeUnit;

import Constant.constant;
import Interface.dataSuccess;
import Model.getDataSources;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static Constant.constant.*;

public class WebServices extends AppCompatActivity {

    TextView id,name,marks;
    Button result;

    String Studentid,Studentname,Studentmarks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_services);
        id = findViewById(R.id.tv_id_display);
        name = findViewById(R.id.tv_name_display);
        marks = findViewById(R.id.tv_mark_display);
        result = findViewById(R.id.button_result);

        result.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AsyncTaskRunner runner = new AsyncTaskRunner();
                runner.execute();
            }
        });

    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // display a progress dialog for good user experiance
            progressDialog = new ProgressDialog(WebServices.this);
            progressDialog.setMessage("Please Wait");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }


        @Override
        protected String doInBackground(String... strings) {

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100, TimeUnit.SECONDS).build();
            Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(constant.baseUrl).addConverterFactory(GsonConverterFactory.create(gson)).build();
            final dataSuccess requestInterface = retrofit.create(dataSuccess.class);
            final Call<getDataSources> getDataSuccessCall = requestInterface.getDataSuccess();

            getDataSuccessCall.enqueue(new Callback<getDataSources>() {
                @Override
                public void onResponse(Call<getDataSources> call, Response<getDataSources> response) {
                    try
                    {
                        progressDialog.dismiss();
                        if(response.isSuccessful())
                        {
                            Log.e("response", "working");
                            getDataSources student = response.body();
                            Studentid = student.StudentId;
                            Studentname = student.StudentName;
                            Studentmarks = student.StudentMarks;
                            id.setText(Studentid);
                            name.setText(Studentname);
                            marks.setText(Studentmarks);
                        }

                        else
                        {
                            Toast.makeText(WebServices.this, "Please Try again", Toast.LENGTH_SHORT).show();
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<getDataSources> call, Throwable t) {

                    Log.e("failure  " +
                            "","working");
                            progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Check your internet COnnection",Toast.LENGTH_LONG).show();

                }
            });

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            
        }
    }

}
