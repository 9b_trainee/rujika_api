package Interface;

import Model.getDataSources;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface dataSuccess {

    @GET("RetrofitAndroidObjectResponse")
    Call<getDataSources> getDataSuccess();
}
