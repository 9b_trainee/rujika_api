package com.internship.a9brainza;

import android.graphics.Color;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class otpActivity extends AppCompatActivity {

    boolean txt1,txt2,txt3,txt4;
    TextView t1,t2,t3,t4,n0,n1,n2,n3,n4,n5,n6,n7,n8,n9;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        txt1 = false;
        txt2 = false;
        txt3 = false;
        txt4 = false;
        final TextView mText = findViewById(R.id.mTextView);
        t1 = findViewById(R.id.pin_first_edittext);
        t2 = findViewById(R.id.pin_second_edittext);
        t3 = findViewById(R.id.pin_third_edittext);
        t4 = findViewById(R.id.pin_forth_edittext);
        n0 = findViewById(R.id.declineButton0);
        n1 = findViewById(R.id.declineButton1);
        n2 = findViewById(R.id.declineButton2);
        n3 = findViewById(R.id.declineButton3);
        n4 = findViewById(R.id.declineButton4);
        n5 = findViewById(R.id.declineButton5);
        n6 = findViewById(R.id.declineButton6);
        n7 = findViewById(R.id.declineButton7);
        n8 = findViewById(R.id.declineButton8);
        n9 = findViewById(R.id.declineButton9);
        new CountDownTimer(90000, 10000) {

            public void onTick(long millisUntilFinished) {
                mText.setText((millisUntilFinished/60000)+":"+(millisUntilFinished/5000));
            }
            public void onFinish() {
                mText.setText("Time Over!");
            }
        }.start();

      n0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt1 == false)
                {
                    t1.setText(n0.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n0.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n0.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n0.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });
      n1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt1 == false)
                {
                    t1.setText(n1.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n1.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n1.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n1.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt1 == false)
                {
                    t1.setText(n2.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n2.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n2.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n2.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt1 == false)
                {
                    t1.setText(n3.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n3.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n3.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n3.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt1 == false)
                {
                    t1.setText(n4.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n4.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n4.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n4.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt1 == false)
                {
                    t1.setText(n5.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n5.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n5.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n5.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(txt1 == false)
                {
                    t1.setText(n6.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n6.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n6.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n6.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txt1 == false)
                {
                    t1.setText(n7.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n7.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n7.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n7.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(txt1 == false)
                {
                    t1.setText(n8.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n8.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n8.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n8.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        n9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(txt1 == false)
                {
                    t1.setText(n9.getText().toString());
                    txt1 = true;
                }
                else if(txt2 == false)
                {
                    t2.setText(n9.getText().toString());
                    txt2 = true;
                }
                else if(txt3 == false)
                {
                    t3.setText(n9.getText().toString());
                    txt3 = true;
                }
                else {
                    if(txt4 == false)
                    {
                        t4.setText(n9.getText().toString());
                        txt4 = true;
                    }
                }
                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

        TextView clearText = findViewById(R.id.backButton);
        clearText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("click", "working");
                if(txt4 == true)
                {
                    t4.setText("");
                    txt4 = false;
                }
                else if(txt3 == true)
                {
                    t3.setText("");
                    txt3 = false;
                }
                else if(txt2 == true)
                {
                    t2.setText("");
                    txt2 = false;
                }
                else
                {
                    if(txt1 == true)
                    {
                        t1.setText("");
                        txt1 = false;
                    }
                }

                if(txt1 == true && txt2 == true && txt3 == true && txt4 == true)
                {
                    Toast.makeText(getApplicationContext(),"Entered OTP is :"+t1.getText().toString()+t2.getText().toString()+t3.getText().toString()+t4.getText().toString(),Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}
